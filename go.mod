module graphschemeanalytics

go 1.17

require (
    git.science.uu.nl/graphpolaris/query-execution v0.0.0-20210913150324-faf925ebb2fb
)
