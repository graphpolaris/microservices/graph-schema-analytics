package main

import (
	"encoding/json"
	"graphschemeanalytics/entity"
	"io/ioutil"
	"log"
	"os"

	"git.science.uu.nl/graphpolaris/query-execution/arangodb"
)

func main() {
	jsonFile, err := os.Open("../test.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		log.Println(err)
	}
	log.Println("Successfully Opened users.json")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
	js, _ := ioutil.ReadAll(jsonFile)
	var inc entity.Schema
	json.Unmarshal(js, &inc)
	arangodbService := arangodb.NewService()

}
