package entity

// IncomingQueryJSON describes the query coming into the service in JSON format
type Schema struct {
	Type  string
	Value SchemaValueStruct
}

// QueryReturnStruct holds the indices of the entities and relations that need to be returned
type SchemaValueStruct struct {
	Nodes []NodeStruct
	Edges []EdgeStruct
	//Modifiers []int
}

// QueryEntityStruct encapsulates a single entity with its corresponding constraints
type NodeStruct struct {
	Name       string
	Attributes []AttributeStruct
}

// QueryRelationStruct encapsulates a single relation with its corresponding constraints
type EdgeStruct struct {
	Name       string
	Collection string
	From       string
	To         string
	Attributes []AttributeStruct
}

// QueryModifierStruct encapsulates a single modifier with its corresponding constraints
type QueryModifierStruct struct {
	Type           string // SUM COUNT AVG
	SelectedType   string // node relation
	SelectedTypeID int    // ID of the enitity or relation
	AttributeIndex int    // = -1 if its the node or relation, = > -1 if an attribute is selected
}

// QuerySearchDepthStruct holds the range of traversals for the relation
type AttributeStruct struct {
	Name string
	Type string
}
